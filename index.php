<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

use Symfony\Component\Dotenv\Dotenv;

require_once __DIR__.'/vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env');

require_once __DIR__.'/app/routes.php';

