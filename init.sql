CREATE TABLE IF NOT EXISTS `shortener_url`.`users` (
    `id` VARCHAR(255) NOT NULL,
    UNIQUE `users_id_unque` (`id`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `shortener_url`.`urls` (
    `id` VARCHAR(36) NOT NULL,
    `userId` VARCHAR(255) NOT NULL,
    `url` VARCHAR(255) NOT NULL,
    `shortUrl` VARCHAR(255) NOT NULL,
    `hits` INT NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
    CONSTRAINT fk_userId
    FOREIGN KEY (userId)
    REFERENCES users (id)
    ON DELETE CASCADE
) ENGINE = InnoDB;