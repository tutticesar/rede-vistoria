<?php

use Symfony\Component\VarDumper\Server\Connection;
require_once 'DB.php';

class Persist
{
    private $action;

    private $table;

    private $data;

    private $connection;

    public function __construct()
    {
        $this->connection = new DB();
    }

    public function setData(array $payload)
    {
        $this->action = $payload['action'];
        $this->table  = $payload['table'];
        $data = (array) $payload['data'];
        $this->data = $data;

        return $this;
    }

    public function execute()
    {
        switch ($this->action) {
            case 'HitUrl':
                $this->hitUrl();
                break;
        }
    }

    private function hitUrl()
    {
        $hit = $this->data['hits'];
        $id = $this->data['id'];

        $sql = "UPDATE {$this->table} SET hits = {$hit} WHERE id = '{$id}'";

        $this->dbexecute($sql);
    }

    private function dbexecute($sql)
    {
        try {
            echo "Executing sql {$sql}" . PHP_EOL;
            $this->connection->exec($sql);
        } catch (\PDOException $th) {
            $this->log($th->getMessage());
        }
    }

    private function log($msg)
    {
        $log = fopen("log.txt", "w");
        fwrite($log, "{$msg}\n");
        fclose($log);
    }
}
