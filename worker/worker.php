<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\Dotenv\Dotenv;
use PhpAmqpLib\Connection\AMQPStreamConnection;

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env');

require_once __DIR__.'/Persist.php';
$persist = new Persist();

define('QUEUE_NAME', 'Php queue');

$connection = new AMQPStreamConnection(
    env('RMQ_HOST'),
    env('RMQ_PORT'),
    env('RMQ_USER'),
    env('RMQ_PASS')
);

$channel = $connection->channel();
$channel->queue_declare(QUEUE_NAME, false, false, false, false);

$callback = function($msg) use ($persist) : void {
    echo "[" . date('Y-m-d H:i:s') . "] Processing data..." . PHP_EOL;
    $data = (array) json_decode($msg->body);
    $persist->setData($data)
        ->execute()
    ;
};

$channel->basic_consume(QUEUE_NAME, '', false, true, false, false, $callback);

while($channel->is_consuming()) {
    $channel->wait();
}

$channel->close();
$connection->close();