<?php

class DB 
{  
    static private $conn; 
     
    public function __construct() 
    {
        $host = env('DB_HOST');
        $dbname = env('DB_NAME');
        $username = env('DB_USER');
        $password = env('DB_PASS');

        if(!self::$conn) { 
            $dns = "mysql:host={$host};dbname=$dbname";

	        try {
               self::$conn = new \PDO($dns, $username, $password);
               self::$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			} catch (\PDOException $e) { 
			   die("PDO CONNECTION ERROR: " . $e->getMessage() . PHP_EOL);
			}
    	}
      	return self::$conn;    	    	
    }
	 
	public function beginTransaction() {
		return self::$conn->beginTransaction();
	}
        
	public function commit() {
		return self::$conn->commit();
	}
	
    public function errorCode() {
    	return self::$conn->errorCode();
    }
    
    public function errorInfo() {
    	return self::$conn->errorInfo();
    }
    
    public function exec($statement) {
    	return self::$conn->exec($statement);
    }
    
    public function getAttribute($attribute) {
    	return self::$conn->getAttribute($attribute);
    }

    public function getAvailableDrivers(){
    	return Self::$conn->getAvailableDrivers();
    }
    
	public function lastInsertId($name) {
		return self::$conn->lastInsertId($name);
	}
        
   	
    public function prepare ($statement, $driver_options=false) {
    	if(!$driver_options) $driver_options=array();
    	return self::$conn->prepare($statement, $driver_options);
    }
    
    public function query($statement) {
    	return self::$conn->query($statement);
    }

    public function queryFetchAllAssoc($statement) {
    	return self::$conn->query($statement)->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    public function queryFetchRowAssoc($statement) {
    	return self::$conn->query($statement)->fetch(\PDO::FETCH_ASSOC);    	
    }
    
    public function queryFetchColAssoc($statement) {
    	return self::$conn->query($statement)->fetchColumn();    	
    }
    
    public function quote ($input, $parameter_type=0) {
    	return self::$conn->quote($input, $parameter_type);
    }
    
    public function rollBack() {
    	return self::$conn->rollBack();
    }      
    
    public function setAttribute($attribute, $value  ) {
    	return self::$conn->setAttribute($attribute, $value);
    }
}