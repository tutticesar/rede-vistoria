<?php

function env($key, $alias = false)
{
    return isset($_ENV[$key]) ? $_ENV[$key] : $alias; 
}