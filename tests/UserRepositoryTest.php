<?php declare(strict_types=1);

use App\Repositories\UserRepository;
use App\Services\SendService;
use App\Repositories\DB;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

final class UserRepositoryTest extends TestCase 
{

    public function testShouldNotCreateADuplicatedUser()
    {
        $db = new DB();
        $faker = Factory::create();
        $userData = ['id' => str_replace(' ', '-', unaccent($faker->name))];

        $repository = new UserRepository($db);
        $repository->save($userData);

        $this->expectException(\PDOException::class);

        $repository->save($userData);
    }

    public function testShouldCreateAnUser()
    {
        $db = new DB();
        $faker = Factory::create();
        $userData = ['id' => str_replace(' ', '-', unaccent($faker->name))];

        $repository = new UserRepository($db);
        $data = $repository->save($userData);

        $this->assertTrue($data);
    }

    public function testShouldFetchAnUser()
    {
        $db = new DB();
        $faker = Factory::create();
        $userData = ['id' => str_replace(' ', '-', unaccent($faker->name))];

        $repository = new UserRepository($db);
        $repository->save($userData);

        $data = $repository->findBy('id', $userData['id']);

        $this->assertEquals($data, $userData);
    }

    public function testShouldDeleteUser()
    {
        $db = new DB();
        $faker = Factory::create();
        $userData = ['id' => str_replace(' ', '-', unaccent($faker->name))];

        $repository = new UserRepository($db);
        $data = $repository->save($userData);

        $deleted = $repository->delete($userData['id']);

        $this->assertEquals($deleted, 1);
    }

    public function testShouldNotDeleteANotFoundUser()
    {
        $db = new DB();
        $faker = Factory::create();
        $userData = ['id' => str_replace(' ', '-', unaccent($faker->name))];

        $repository = new UserRepository($db);

        $deleted = $repository->delete($userData['id']);

        $this->assertEquals($deleted, 0);
    }
}
