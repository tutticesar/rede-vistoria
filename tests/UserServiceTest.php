<?php declare(strict_types=1);

use App\Exceptions\BadRequestException;
use App\Exceptions\ConflictException;
use App\Exceptions\UserNotFoundException;
use App\Repositories\DB;
use App\Repositories\StatsRepository;
use App\Repositories\UrlRepository;
use App\Repositories\UserRepository;
use App\Services\SendService;
use App\Services\StatsService;
use App\Services\UrlService;
use App\Services\UserService;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{

    private $service;

    protected function setUp()
    {
        $db = new DB();
        $repository = new UserRepository( $db );
        $urlRepository = new UrlRepository( $db );
        $statsRepository = new StatsRepository( $db );
        $sender     = new SendService();
        $statsService = new StatsService($statsRepository);
        $urlService = new UrlService( $urlRepository, $sender );
        $this->service =  new UserService($repository, $urlService, $statsService);
    }

    public function testShouldCreateAnUser()
    {
        $faker = Factory::create();
        $userData = ['id' => $faker->name];

        $result = $this->service->save($userData);

        $this->assertEquals(
            $result['id'],
            str_replace(' ', '-', unaccent($userData['id']))
        );
    }

    public function testSouldNotCreateADulicatedUser()
    {
        $faker = Factory::create();
        $userData = ['id' => $faker->name];

        $this->service->save($userData);

        $this->expectException(ConflictException::class);

        $this->service->save($userData);
    }

    public function testShouldNotCreateAnUrlForNonCreatedUser()
    {
        $faker = Factory::create();
        $user = ['id' => $faker->name];
        
        $url = $faker->url;

        $this->expectException(UserNotFoundException::class);

        $this->service->createUrl($user, $url);
    }

    public function testShouldNotCreateAnInvalidOrNullUrl()
    {
        $faker = Factory::create();
        $user = ['id' => $faker->name];
        
        $url = $faker->url;

        $this->expectException(BadRequestException::class);

        $this->service->createUrl($user, null);
    }

    public function testShouldCreateAnUrlForUser()
    {
        $faker = Factory::create();
        $userData = ['id' => $faker->name];
        
        $userData = $this->service->save($userData);

        $url = $faker->url;

        $created = $this->service->createUrl($userData, $url);

        $this->assertIsArray($created);
    }

    public function testShouldDeleteAnUser()
    {
        $faker = Factory::create();
        $userData = ['id' => $faker->name];
        
        $userData = $this->service->save($userData);

        $deleted = $this->service->delete($userData['id']);

        $this->assertEquals($deleted, 1);
    }

    public function testShouldNotDeleteANotFoundUser()
    {
        $faker = Factory::create();
        $userData = ['id' => $faker->name];
        
        $this->expectException(UserNotFoundException::class);

        $this->service->delete($userData['id']);

    }
}