<?php
namespace App\Controllers;
use App\Services\SendService;
use App\Services\UserService;

class UserController
{

    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function store()
    {
        $id = input('id');

        try {
            $res = $this->userService->save(compact('id'));
            return response()->httpCode(201)->json($res); 
        } catch (\Throwable $e) {
            
            return response()
                ->httpCode( $e->getCode() )
                ->json( getExceResponse($e) )
            ;
        }
    }

    public function createUrl($userId)
    {
        $url = input('url');

        try {
            $res = $this->userService->createUrl(['id'=>$userId], $url);
            return response()->httpCode(201)->json($res); 
        } catch (\Throwable $e) {
            return response()
                ->httpCode( $e->getCode() )
                ->json( getExceResponse($e) )
            ;
        }
    }

    public function urls(string $userId)
    {
        try {
            $data = $this->userService->getUrls($userId);
            return response()->json($data);
        } catch (\Throwable $th) {
            return response()
                ->httpCode( $th->getCode() )
                ->json( getExceResponse($th) )
            ;
        }
    }

    public function stats(string $userId)
    {
        try {
            $data = $this->userService->stats($userId);
            return response()->json($data);
        } catch (\Throwable $th) {
            return response()
                ->httpCode( $th->getCode() )
                ->json( getExceResponse($th) )
            ;
        }
    }

    public function delete($userId)
    {
        try {
            $this->userService->delete($userId);
            return response()->httpCode(204);
        } catch (\Throwable $th) {
            return response()
                ->httpCode( $th->getCode() )
                ->json( getExceResponse($th) )
            ;
        }
    }
}