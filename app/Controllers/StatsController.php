<?php
namespace App\Controllers;

use App\Services\StatsService;

class StatsController
{
    public $service;

    public function __construct(StatsService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        try {
            $data = $this->service->get();
            return response()->json($data);
        } catch (\Throwable $th) {
            return response()
                ->httpCode( $th->getCode() )
                ->json( getExceResponse($th) )
            ;
        }
    }

    public function show($urlId)
    {
        try {
            $data = $this->service->find($urlId);

            return response()->json($data);
        } catch (\Throwable $th) {
            return response()
                ->httpCode( $th->getCode() )
                ->json( getExceResponse($th) )
            ;
        }
    }
}