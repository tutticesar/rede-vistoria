<?php
namespace App\Controllers;
use App\Services\UrlService;

class UrlController 
{
    private const DOMAIN = 'http://art.ur/';

    private $service;

    public function __construct(UrlService $urlService) 
    {
        $this->service = $urlService;
    }

    public function index($url)
    {
        try {
            
            $found = $this->service->find(self::DOMAIN . $url);
            return response()->httpCode( 301 )
                ->json(['url'=>$found]);

        } catch (\Throwable $e) {
            return response()
                ->httpCode( $e->getCode() )
                ->json( getExceResponse($e) );
        }

    }

    public function delete($urlId)
    {
        try {
            $found = $this->service->delete($urlId);
            return response()->httpCode( 204 );
        } catch (\Throwable $e) {
            return response()
                ->httpCode( $e->getCode() )
                ->json( getExceResponse($e) );
        }
    }
}