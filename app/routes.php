<?php
use FastRoute\RouteCollector;

use DI\ContainerBuilder;

$containerBuilder = new ContainerBuilder;
$container = $containerBuilder->build();

$dispatcher = FastRoute\simpleDispatcher(function (RouteCollector $r) {

    $r->addRoute('GET', '/', function() {
        return response()->json('Application is alive!');
    }); 

    $r->addRoute('GET', '/healthy', function() {
        return response()->json('Application is alive!');
    }); 

    $r->addRoute('POST', '/users', ['App\Controllers\UserController', 'store']);
    $r->addRoute('POST', '/users/{userId}/urls', ['App\Controllers\UserController', 'createUrl']);
    $r->addRoute('DELETE', '/users/{userId}', ['App\Controllers\UserController', 'delete']);
    $r->addRoute('GET', '/users/{userId}/urls', ['App\Controllers\UserController', 'urls']);
    $r->addRoute('GET', '/users/{userId}/stats', ['App\Controllers\UserController', 'stats']);

    $r->addRoute('GET', '/stats', ['App\Controllers\StatsController', 'index']); 
    $r->addRoute('GET', '/stats/{urlId}', ['App\Controllers\StatsController', 'show']); 
    
    $r->addRoute('DELETE', '/urls/{urlId}', ['App\Controllers\UrlController', 'delete']);
    $r->addRoute('GET', '/{url}', ['App\Controllers\UrlController', 'index']);
});

$route = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);

switch ($route[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        echo '404 Not Found';
        break;

    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        echo '405 Method Not Allowed';
        break;

    case FastRoute\Dispatcher::FOUND:
        $controller = $route[1];
        $parameters = $route[2];
        $container->call($controller, $parameters);
        break;
}