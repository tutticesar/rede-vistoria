<?php
namespace App\Repositories;

class StatsRepository
{
    private $database;

    public function __construct(DB $database)
    {
        $this->database = $database;
    }

    public function get()
    {
        $sql = "SELECT id, hits, url, shortUrl FROM urls";
 
        $data = $this->database->queryFetchAllAssoc($sql);

        if (empty($data)) return null;
        return $data;
    }

    public function getByUserId($userId)
    {
        $sql = "SELECT id, hits, url, shortUrl FROM urls WHERE userId = '{$userId}'";
        
        $data = $this->database->queryFetchAllAssoc($sql);
        if (empty($data)) return null;
        return $data;
    }

    public function find($urlId)
    {
        $sql = "SELECT id, hits, url, shortUrl FROM urls  WHERE id = '{$urlId}' LIMIT 1";

        $data = $this->database->queryFetchAllAssoc($sql);

        if (empty($data)) return null;
        return reset($data);
    }

}