<?php
namespace App\Repositories;
use App\Repositories\DB;

class UrlRepository
{
    private $database;

    public function __construct(DB $database)
    {
        $this->database = $database;
    }

    public function findBy($field, $value)
    {
        $data = $this->database->queryFetchAllAssoc("SELECT * FROM urls WHERE {$field} = '{$value}'");
        if (empty($data)) return null;
        return reset($data);
    }

    public function save($data)
    {
        $insertSqlString = $this->getInsertSqlString($data);
        $stmt = $this->database->prepare($insertSqlString);
        return $stmt->execute();
    }

    public function getByUserId($userId)
    {
        $data = $this->database->queryFetchAllAssoc("SELECT * FROM urls WHERE userId = '{$userId}'");
        if (empty($data)) return null;
        return $data;
    }

    private function getInsertSqlString(array $data) : string
    {
        $fields = implode(',', array_keys($data));
        $values = array_values($data);

        $sql = "INSERT INTO urls ($fields) VALUES (";

        $end = count($values) - 1;

        foreach ($values as $i => $value) {
            $sql .= "'{$value}'" . ( $i < $end ? ',' : ')');
        }

        return $sql;
    }

    public function delete(string $id)
    {
        $stmt = $this->database->prepare("DELETE FROM urls WHERE id = '{$id}'");
        $stmt->execute();   
        return $stmt->rowCount();
    }
}