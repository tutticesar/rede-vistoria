<?php
namespace App\Services;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class SendService {

    private const QUEUE_NAME = 'Php queue';

    private $connection;
    
    private $channel;

    public function __construct()
    {
        $this->connect();
    }

    private function connect()
    {
        $this->connection = new AMQPStreamConnection(
            env('RMQ_HOST'),
            env('RMQ_PORT'),
            env('RMQ_USER'),
            env('RMQ_PASS')
        );
    
        $this->channel = $this->connection->channel();
    }

    public function send(string $message)
    {
        $this->connect();
        $this->channel->queue_declare(self::QUEUE_NAME, false, false, false, false);

        $msg = new AMQPMessage($message);
        
        $this->channel->basic_publish($msg, '', self::QUEUE_NAME);
        
        $this->channel->close();
        $this->connection->close();
    }

}