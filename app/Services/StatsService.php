<?php
namespace App\Services;

use App\Exceptions\UrlNotFoundException;
use App\Repositories\StatsRepository;

class StatsService
{

    private $repository;

    public function __construct(StatsRepository $statsRepository)
    {
        $this->repository = $statsRepository;
    }

    public function get()
    {
        $data = $this->repository->get();
        return $this->prepareOutPutData($data);
    }

    public function getByUserId(string $userId)
    {
        $data = $this->repository->getByUserId($userId);
        return $this->prepareOutPutData($data);
    }

    public function find($urlId)
    {
        $data = $this->repository->find($urlId);

        if (empty($data))
            throw new UrlNotFoundException("Url with ID {$urlId} wasn't found!");

        return $data;
    }

    private function prepareOutPutData(array $data): array
    {
        $hits = array_sum(array_map(function($item) { 
            return $item['hits']; 
        }, $data));

        $urlCount = count($data);
 
        $topUrls = $this->bubbleSortTopUrl($data);

        return [compact('hits', 'urlCount', 'topUrls')];
    }

    private function bubbleSortTopUrl($hash) 
    {
        $newHash = [];
        $amount = count($hash);
        for ($i = 0; $i < $amount; $i++) {
            
            for ($j=0; $j < ($amount - 1); $j++) { 
                if ($hash[$j + 1]['hits'] <= $hash[$j]['hits']) {
                    $aux = $hash[$j];
                    $hash[$j]=$hash[$j+1];
                    $hash[$j+1] = $aux;
                }
            }

            $newHash[$i]=$aux;
        }

        return $newHash;
    }
}