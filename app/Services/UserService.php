<?php
namespace App\Services;

use App\Exceptions\BadRequestException;
use App\Exceptions\ConflictException;
use App\Exceptions\UserNotFoundException;
use App\Repositories\UserRepository;

class UserService 
{
    private $repository;
    
    private $urlService;

    private $statsService;
    
    public function __construct( 
        UserRepository $repository,
        UrlService $urlService,
        StatsService $statsService
    )
    {
        $this->repository = $repository;
        $this->urlService = $urlService;
        $this->statsService = $statsService;
    }

    public function save(array $data)
    {

        $id = str_replace(' ', '-', unaccent($data['id']));
        //check duplicate
        $found = $this->repository->findBy('id', $id);

        if ($found)
            throw new ConflictException("User with ID: {$id} already exists!");

        $data['id'] = $id;
        if ($this->repository->save($data)) {
            return $data;
        }

        return false;

    }

    public function createUrl($userData, $url)
    {

        if (!$url || !filter_var($url, FILTER_VALIDATE_URL))
            throw new BadRequestException("Invalid entry for URL: {$url}");

        $id = str_replace(' ', '-', unaccent($userData['id']));

        $found = $this->repository->findBy('id', $id);

        if (!$found)
            throw new UserNotFoundException("User with ID: {$id} wasn't found!");

        return $this->urlService->save($url, $userData['id']);
    }

    public function getUrls($userId)
    {
        $found = $this->repository->findBy('id', $userId);

        if (!$found)
            throw new UserNotFoundException("User with ID: {$userId} wasn't found!");

        return $this->urlService->getByUserId($userId);
    }

    public function stats($userId)
    {
        $found = $this->repository->findBy('id', $userId);

        if (!$found)
            throw new UserNotFoundException("User with ID: {$userId} wasn't found!");

        return $this->statsService->getByUserId($userId);
    }

    public function delete(string $id)
    {
        $found = $this->repository->findBy('id', $id);

        if (!$found)
            throw new UserNotFoundException("User with ID {$id} wasn't found");
            
        return $this->repository->delete($id);
    }
}