<?php
namespace App\Services;

class ShortenerService
{

  private $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  
  private $padding = 12;
  
  private $salt = 'zWsjk2eZM99zManyk0a91';

  public function encode($n) {
    $k = 0;

    if ($this->padding > 0 && !empty($this->salt)) {
        $k = self::getSeed($n, $this->salt, $this->padding);
        $n = (int)($k.$n);
    }

    return self::numToAlpha($n, $this->chars);
  }

  public function decode($s) {
    $n = self::alphaToNum($s, $this->chars);

    return (!empty($this->salt)) ? substr($n, $this->padding) : $n;
  }

  public static function getSeed($n, $salt, $padding) {
    $hash = md5($n.$salt);
    $dec = hexdec(substr($hash, 0, $padding));
    $num = $dec % pow(10, $padding);
    if ($num == 0) $num = 1;
    $num = str_pad($num, $padding, '0');

    return $num;
  }

  public static function numToAlpha($n, $s) {
    $b = strlen($s);
    $m = $n % $b;

    if ($n - $m == 0) return substr($s, $n, 1);

    $a = '';

    while ($m > 0 || $n > 0) {
      $a = substr($s, $m, 1).$a;
      $n = ($n - $m) / $b;
      $m = $n % $b;
    }

    return $a;
  }

  public static function alphaToNum($a, $s) {
    $b = strlen($s);
    $l = strlen($a);

    for ($n = 0, $i = 0; $i < $l; $i++) {
        $n += strpos($s, substr($a, $i, 1)) * pow($b, $l - $i - 1);
    }

    return $n;
  }
}