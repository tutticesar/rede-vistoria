<?php
namespace App\Services;

use App\Exceptions\UrlNotFoundException;
use App\Repositories\UrlRepository;
use App\Services\ShortenerService;
use Ramsey\Uuid\Uuid;

class UrlService
{

    private $repository;

    private $sender;

    public function __construct(UrlRepository $repository, SendService $sendService)
    {
        $this->repository = $repository;
        $this->sender = $sendService;
    }

    public function find($url)
    {
        $found = $this->repository->findBy('shortUrl', $url);

        if (!$found)
            throw new UrlNotFoundException("URL {$url} wasn't found!");
        
        $found['hits'] += 1;

        $this->sender->send(json_encode([
            'action' => 'HitUrl',
            'table' => 'urls',
            'data' => $found
        ]));

        return $found;
    }

    public function getByUserId($userId)
    {
        $found = $this->repository->getByUserId($userId);
        return $found;
    }

    public function save($url, $userId)
    {
        $shortenerService = new ShortenerService();
        $shortUrl = $shortenerService->encode($url);

        $data = [
            'id' => Uuid::uuid4()->toString(),
            'userId' => $userId,
            'url' => $url,
            'shortUrl' => "http://art.ur/{$shortUrl}",
            'hits' => 0
        ];

        try {
            $this->repository->save($data);
            unset($data['userId']);
            return $data;
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }

    }

    public function delete(string $id)
    {
        $found = $this->repository->findBy('id', $id);

        if (!$found)
            throw new UrlNotFoundException("Url with ID {$id} wasn't found");
            
        return $this->repository->delete($id);
    }
}