<?php
namespace App\Exceptions;

class ConflictException extends \Exception 
{
    public function __construct($message, $code = 409, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}