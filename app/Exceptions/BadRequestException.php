<?php
namespace App\Exceptions;

class BadRequestException extends \Exception 
{
    public function __construct($message = "Unprocessable entity", $code = 422, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}