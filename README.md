# Artur Cesar's Assessment Test.

### Made with:
- Docker
- Php@7.4
- RabbitMQ
- Mysql@5.7

## Estructure:
```php
\application
    \app
        \Controllers
        \Exceptions
        \Repositories
        \Services
        index.php
    \helpers
    \worker
    \testes
```
## Worker
This folder contains another "project" to consum the queues on rabbitmq. As a improovement, this folder can be isolated in a different reposistory, as a microsservice.

### Running the project:
1. First, you need to run this commands.
```bash
$ git clone
$ cd rede-vistorias/
$ docker-compose up -d
```
2. Configure .env file for on `/app/.env` and `/app/worker/.env` 
```
DB_HOST=mysql
DB_NAME=shortener_url
DB_USER=redevistoria
DB_PASS=123456

RMQ_PORT=5672
RMQ_HOST=redevistoria_rabbitmq
RMQ_USER=mqadmin
RMQ_PASS=mqadmin
```
Now is possible to access the **rabbitmq dashboard** on `http://localhost:15672` and **phpMyAdmin** on `https://localhost:8081`.

3. phpMyAdmin:
Inside the `shortener_url` database, copy the init.sql file's content and run. This will create the tables, just as long as i'm not using frameworks. 

4. access such as application as application_worker container and run
```bash
$ composer install
```

**DONE**
The API will be listen at `http://localhost:81`

### tests
Inside the application container, just run:
```bash
$ ./vendor/bin/phpunit --testdox tests
```

Best regards!